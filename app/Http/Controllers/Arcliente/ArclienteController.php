<?php

namespace App\Http\Controllers\Arcliente;

use App\Models\Arcliente;
use App\Models\CONSULTAS_LOG;
use App\Querys\ConsultasLogQuery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ArclienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consultarUsuario(Request $request) {
        //dd($request->cedula_ruc);
        $cliente = Arcliente::where('CEDULA', $request->cedula_ruc)
            ->orWhere('RUC', $request->cedula_ruc)
            ->first();

        if (!is_null($cliente)) {
            $user = Auth::user();
            if (!ConsultasLogQuery::existe_consulta_log($user, $cliente)) {//Si ya existe la consulta, no se la vuelve a guardar
                if (!ConsultasLogQuery::crear_consulta_log($user, $cliente)) {
                    dd("ERROR AL CREAR CONSULTA_LOG");
                }
            }
        }

        //dd($user->id);
        //$crear_consulta_log = ConsultasLogQuery::crear_consulta_log($user);

        return view('home')->with('cliente', $cliente);
    }

    public function historialConsultas() {
        return view('consulta.usuarios');
    }

    public function consultarUsuarios(Request $request) {
        $initial_date_formatted = date_create_from_format('d/m/Y', $request->fecha_inicio);
        $fecha_inicio = $initial_date_formatted->format('Y-m-d');

        $end_date_formatted = date_create_from_format('d/m/Y', $request->fecha_fin);
        $fecha_fin = $end_date_formatted->format('Y-m-d');


        $cliente = Auth::user();
        $data = CONSULTAS_LOG::whereBetween('fecha_consulta', array($fecha_inicio, $fecha_fin))
            ->where('idUsuario', $cliente->id)
            ->get();

        //dd($data);

        return view('consulta.usuarios')->with(
            [
                'data' => $data,
                'fecha_inicio' => $fecha_inicio,
                'fecha_fin' => $fecha_fin,
            ]);
    }
}
