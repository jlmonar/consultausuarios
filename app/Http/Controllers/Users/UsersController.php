<?php

namespace App\Http\Controllers\Users;

use App\Exports\UsersExport;
use App\Exports\UsuariosExport;
use App\Models\CONSULTAS_LOG;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

use Spatie\ArrayToXml\ArrayToXml;

use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export($fecha_inicio, $fecha_fin) {
        return Excel::download(new UsuariosExport($fecha_inicio, $fecha_fin), 'users.csv');
    }

    public function downloadXml($fecha_inicio, $fecha_fin) {
        $logged_user = Auth::user();
        $data = CONSULTAS_LOG::whereBetween('fecha_consulta', array($fecha_inicio, $fecha_fin))
            ->where('idUsuario', $logged_user->id)
            ->get();

        //Nuevo XML Document
        $xmlDoc = new \DOMDocument();

        //Creo elemento raíz
        $root = $xmlDoc->appendChild($xmlDoc->createElement("CONSULTAS"));

        $xmlDoc->formatOutput = true;

        foreach($data as $item)
        {
            $usuarioTag = $root->appendChild($xmlDoc->createElement("USUARIO"));

            $datosPersonalesTag = $usuarioTag->appendChild($xmlDoc->createElement("DATOS_PERSONALES"));

            $datosPersonalesTag->appendChild($xmlDoc->createElement("NOMBRE", $item->cliente->NOMBRE));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("APELLIDO", $item->cliente->APELLIDO));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("CEDULA", $item->cedula));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("RUC", $item->ruc));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("CIUDAD", $item->cliente->CIUDAD));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("DOMICILIO", $item->cliente->DOMICILIO));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("TELEFONOS", $item->cliente->TELEFONOS));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("CELULAR", $item->cliente->CL_CELULAR));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("EMAIL", $item->cliente->MAIL));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("EMAIL_TRABAJO", $item->cliente->CL_MAILTRABAJO));
            $datosPersonalesTag->appendChild($xmlDoc->createElement("FECHA_CONSULTA", $item->fecha_consulta));
        }

        $xmlDoc->save("xml/usuario.xml");

        return response()->download('xml/usuario.xml')->deleteFileAfterSend(true);
    }
}
