<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CONSULTAS_LOG extends Model
{
    protected $table = 'CONSULTAS_LOG';

    protected $primaryKey = 'id';

    public  $timestamps = false;

    public function cliente() {
        if (strlen($this->cedula) == 10) {
            return $this->hasOne('App\Models\Arcliente', 'CEDULA', 'cedula');
        } else if (strlen($this->ruc) == 13) {
            return $this->hasOne('App\Models\Arcliente', 'RUC', 'ruc');
        }
    }
}
