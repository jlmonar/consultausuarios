<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arcliente extends Model
{
    protected $table = 'ARCLIENTE';

    protected $primaryKey = 'CODIGO';

    public  $timestamps = false;
}
