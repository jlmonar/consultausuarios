<?php

namespace App\Querys;

use App\Models\CONSULTAS_LOG;
use DateTime;

class ConsultasLogQuery
{
    public static function crear_consulta_log($user, $cliente) {
        try {
            $consulta_log = new CONSULTAS_LOG();
            $consulta_log->idUsuario = $user->id;
            $consulta_log->cedula = $cliente->CEDULA;
            $consulta_log->ruc = $cliente->RUC;
            $consulta_log->fecha_consulta = date_format(new DateTime(), 'Y/m/d H:i:s');

            $consulta_log->save();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public static function existe_consulta_log($user, $cliente) {
        try {
            if (strlen($cliente->CEDULA) == 10) {
                $consulta_log = CONSULTAS_LOG::where('idUsuario', $user->id)
                    ->where('CEDULA', $cliente->CEDULA)
                    ->first();
            } else if (strlen($cliente->RUC) == 13) {
                $consulta_log = CONSULTAS_LOG::where('idUsuario', $user->id)
                    ->where('RUC', $cliente->RUC)
                    ->first();
            }

            if (is_null($consulta_log)) {
                return false;
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}