<?php

namespace App\Exports;

use App\Models\CONSULTAS_LOG;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class UsuariosExport implements FromView
{
    private $fecha_inicio;
    private $fecha_fin;

    public function __construct($fecha_inicio, $fecha_fin)
    {
        $this->fecha_inicio = $fecha_inicio;
        $this->fecha_fin = $fecha_fin;
    }

    public function view(): View
    {
        $cliente = Auth::user();
        $data = CONSULTAS_LOG::whereBetween('fecha_consulta', array($this->fecha_inicio, $this->fecha_fin))
            ->where('idUsuario', $cliente->id)
            ->get();

        return view('exports.usuarios', [
            'data' => $data
        ]);
    }
}
