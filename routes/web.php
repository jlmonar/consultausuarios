<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

//MIDDLEWARES
Route::middleware(['auth'])->group(function() {
    /* Arcliente */
    Route::resource('usuarios', 'Arcliente\ArclienteController', ['only' => ['index', 'show']]);
    Route::get('consulta_usuario', 'Arcliente\ArclienteController@consultarUsuario')->name('consultar-usuario');

    Route::get('historial_consultas', 'Arcliente\ArclienteController@historialConsultas')->name('historial-consultas');
    Route::get('consultar_usuarios', 'Arcliente\ArclienteController@consultarUsuarios')->name('consultar-usuarios');

    Route::get('download_csv/{fecha_inicio}/{fecha_fin}/', 'Users\UsersController@export')->name('download-csv');
    Route::get('download_xml/{fecha_inicio}/{fecha_fin}/', 'Users\UsersController@downloadXml')->name('download-xml');
});


