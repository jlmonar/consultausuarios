@extends('layouts.app')

@section('content')
<div class="container">

    <div class="form-group">
        <div class="row">
            {!! Form::model(Request::all(), ['route' => 'consultar-usuario', 'method' => 'GET']) !!}
            <div class="col-xs-8 col-sm-8 col-md-8">
                {!!Form::text('cedula_ruc', null, ['class' => 'form-control', 'placeholder' => 'Cédula/RUC', 'aria-descridbedby' => 'search', 'required' => true]) !!}
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <button type="submit" class="btn btn-default btn-primary">Buscar</button>
            </div>
            {!!Form::close() !!}
        </div>
        @if(isset($cliente))
            <div class="container jumbotron">
                <h2>DATOS PERSONALES:</h2>
                <h4>Nombre: {{$cliente->NOMBRE}}</h4>
                <h4>Apellido: {{$cliente->APELLIDO}}</h4>
                <h4>Cédula: {{$cliente->CEDULA}}</h4>
                <h4>RUC: {{$cliente->RUC}}</h4>
                <h4>Ciudad: {{$cliente->CIUDAD}}</h4>
                <h4>Domicilio: {{$cliente->DOMICILIO}}</h4>
                <h4>Telefonos: {{$cliente->TELEFONOS}}</h4>
                <h4>Celular: {{$cliente->CL_CELULAR}}</h4>
                <h4>Correo Electrónico: {{$cliente->MAIL}}</h4>
                <h4>Dirección de trabajo: {{$cliente->CL_MAILTRABAJO}}</h4>
            </div>
        @endif
    </div>
</div>
@endsection
