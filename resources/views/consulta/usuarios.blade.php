@extends('layouts.app')

@section('content')
    <!-- Fuente: http://www.daterangepicker.com/ -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <div class="container">
        <div class="form-group">
            <div class="row">
                {!! Form::model(Request::all(), ['route' => 'consultar-usuarios', 'method' => 'GET']) !!}
                <div class="col-xs-12 col-sm-4 col-md-4">
                    {!!Form::label('fecha_inicio_fin', 'Fecha Inicio:', ['class' => 'control-label'])!!}
                    {!!Form::text('fecha_inicio', null, ['class' => 'form-control', 'placeholder' => 'Fecha Inicio...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                    <br>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    {!!Form::label('fecha_inicio_fin', 'Fecha Fin:', ['class' => 'control-label'])!!}
                    {!!Form::text('fecha_fin', null, ['class' => 'form-control', 'placeholder' => 'Fecha Fin...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <br>
                    <button type="submit" class="btn btn-default btn-search-product">Buscar</button>
                </div>
                {!!Form::close() !!}
            </div>
        </div>

        @if (isset($data))
            <div class="panel panel-default" >
                <div class="panel-heading" style="font-weight: bold; font-size: 18px;">
                    Consultas
                    <a class="pull-right" href="{{route('download-csv', [$fecha_inicio, $fecha_fin])}}" target="_blank" title="Descargar CSV" style="margin-right: 10px;">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    </a>
                    <a class="pull-right" href="{{route('download-xml', [$fecha_inicio, $fecha_fin])}}" target="_blank" title="Descargar XML" style="margin-right: 10px;">
                        <i class="fa fa-file-code-o" aria-hidden="true"></i>
                    </a>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Cedula</th>
                        <th>Ruc</th>
                        <th>Nombre</th>
                        <th>Fecha consulta</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td width="10%">{{ $item->id }}</td>
                            <td width="18%">{{$item->cedula}}</td>
                            <td width="18%">{{$item->ruc}}</td>
                            <td width="35%">{{$item->cliente->NOMBRE}} {{$item->cliente->APELLIDO}}</td>
                            <td width="20%">{{$item->fecha_consulta}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <script>
        $('input[name="fecha_inicio"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        $('input[name="fecha_fin"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    </script>
@endsection