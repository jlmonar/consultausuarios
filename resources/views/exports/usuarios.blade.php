<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
</head>
<body>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Cedula</th>
            <th>Ruc</th>
            <th>Nombre</th>
            <th>Ciudad</th>
            <th>Domicilio</th>
            <th>Teléfonos</th>
            <th>Celular</th>
            <th>Correo Electrónico</th>
            <th>Correo Electrónico de trabajo</th>
            <th>Fecha consulta</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{$item->cedula}}</td>
                <td>{{$item->ruc}}</td>
                <td>{{$item->cliente->NOMBRE}} {{$item->cliente->APELLIDO}}</td>
                <td>{{$item->cliente->CIUDAD}}</td>
                <td>{{$item->cliente->DOMICILIO}}</td>
                <td>{{$item->cliente->TELEFONOS}}</td>
                <td>{{$item->cliente->CL_CELULAR}}</td>
                <td>{{$item->cliente->MAIL}}</td>
                <td>{{$item->cliente->CL_MAILTRABAJO}}</td>
                <td>{{$item->fecha_consulta}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>