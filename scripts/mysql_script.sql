CREATE TABLE CONSULTAS_LOG (
	id INT(10) UNSIGNED AUTO_INCREMENT NOT NULL,
	idUsuario INT(10),
	cedula  varchar(10),
	ruc varchar(13),
	fecha_consulta datetime,
	PRIMARY KEY (id),
	FOREIGN KEY (idUsuario) REFERENCES users(id)
);
